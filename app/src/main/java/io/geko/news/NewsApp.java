package io.geko.news;

import android.app.Application;
import android.content.SharedPreferences;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.listener.RequestListener;
import com.facebook.imagepipeline.listener.RequestLoggingListener;
import com.google.gson.Gson;
import io.geko.news.services.mercury.MercuryService;
import io.geko.news.services.qwant.QwantService;
import io.geko.news.services.searx.SearxService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created at 22/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class NewsApp extends Application
{
	@SuppressWarnings( "unused" )
	public static final String TAG = NewsApp.class.getSimpleName();
	public static final Gson GSON = new Gson();

	private static final String SHARED_PREFERENCE_NAME = "io.geko.news.SharedPreference";

	private static OkHttpClient      okHttpClient;
	private static SearxService      searxService;
	private static MercuryService    mercuryService;
	private static SharedPreferences sharedPreferences;
	private static QwantService      qwantService;

	@Override
	public void onCreate()
	{
		super.onCreate();
		initFresco();
		initRetrofit();
		sharedPreferences = getSharedPreferences( SHARED_PREFERENCE_NAME, MODE_PRIVATE );
	}

	public static OkHttpClient getOkHttpClient()
	{
		if ( okHttpClient == null )
		{
			OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
			okHttpClientBuilder.addInterceptor( new HttpLoggingInterceptor().setLevel( HttpLoggingInterceptor.Level.BODY ) );
			okHttpClient = okHttpClientBuilder.build();
		}
		return okHttpClient;
	}

	public static SearxService getSearxService()
	{
		return searxService;
	}

	public static MercuryService getMercuryService()
	{
		return mercuryService;
	}

	public static SharedPreferences getSharedPreferences()
	{
		return sharedPreferences;
	}

	public static QwantService getQwantService()
	{
		return qwantService;
	}

	private void initFresco()
	{
		Set<RequestListener> requestListeners = new HashSet<>();
		requestListeners.add( new RequestLoggingListener() );
//		ImageDecoderConfig imageDecoderConfig = ImageDecoderConfig.newBuilder()
//																  .addDecodingCapability( IcoDecoder.ICO, IcoDecoder.FORMAT_CHECKER, new IcoDecoder() )
//																  .build();

		ImagePipelineConfig imagePipelineConfig = OkHttpImagePipelineConfigFactory.newBuilder( this, getOkHttpClient() )
//																				  .setImageDecoderConfig( imageDecoderConfig )
																				  .setRequestListeners( requestListeners )
																				  .build();
		Fresco.initialize( this, imagePipelineConfig/*, new DraweeConfig.Builder().setDrawDebugOverlay( BuildConfig.DEBUG ).build()*/ );
	}

	private void initRetrofit()
	{
		Retrofit retrofit = new Retrofit.Builder()
				.client( getOkHttpClient() )
				.baseUrl( "https://searx.ch" )
				.addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
				.addConverterFactory( GsonConverterFactory.create() )
				.build();
		searxService = retrofit.create( SearxService.class );

		retrofit = new Retrofit.Builder()
				.client( getOkHttpClient().newBuilder().addInterceptor( chain -> {
					Request.Builder requestBuilder = chain.request().newBuilder();
					requestBuilder.addHeader( "X-API-KEY", BuildConfig.MERCURY_API_KEY );
					Request request = requestBuilder.build();
					return chain.proceed( request );
				} ).build() )
				.baseUrl( "https://mercury.postlight.com" )
				.addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
				.addConverterFactory( GsonConverterFactory.create() )
				.build();
		mercuryService = retrofit.create( MercuryService.class );

		retrofit = new Retrofit.Builder()
				.client( getOkHttpClient() )
				.baseUrl( "https://api.qwant.com/api/search/" )
				.addCallAdapterFactory( RxJava2CallAdapterFactory.create() )
				.addConverterFactory( GsonConverterFactory.create() )
				.build();
		qwantService = retrofit.create( QwantService.class );
	}
}
