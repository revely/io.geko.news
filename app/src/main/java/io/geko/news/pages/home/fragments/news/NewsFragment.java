package io.geko.news.pages.home.fragments.news;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import io.geko.news.R;
import io.geko.news.bindings.variables.Article;
import io.geko.news.databinding.FragmentNewsBinding;
import io.geko.news.databinding.ViewItemNewsBinding;
import io.geko.news.pages.article.ArticleActivity;
import io.geko.news.pages.home.HomeActivity;
import io.geko.news.services.Helper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created at 22/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class NewsFragment extends Fragment
{
	@SuppressWarnings( "unused" )
	public static final String TAG = NewsFragment.class.getSimpleName();

	private FragmentNewsBinding binding;
	private NewsAdapter         adapter;

	public static NewsFragment newInstance()
	{
		Bundle args = new Bundle();
		NewsFragment fragment = new NewsFragment();
		fragment.setArguments( args );
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView( LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState )
	{
		binding = DataBindingUtil.inflate( LayoutInflater.from( getContext() ), R.layout.fragment_news, container, false );
		return binding.getRoot();
	}

	@Override
	public void onViewCreated( View view, @Nullable Bundle savedInstanceState )
	{
		adapter = new NewsAdapter();
		binding.news.setLayoutManager( new LinearLayoutManager( getContext(), LinearLayoutManager.VERTICAL, false ) );
		binding.news.setAdapter( adapter );
		binding.swipeRefreshLayout.setOnRefreshListener( this::onRefresh );
		binding.swipeRefreshLayout.setColorSchemeResources(
				R.color.materialGrey600,
				android.R.color.holo_green_light,
				android.R.color.holo_blue_bright,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light );

	}

	@Override
	public void onStart()
	{
		super.onStart();
		EventBus.getDefault().register( this );
	}

	@Override
	public void onStop()
	{
		super.onStop();
		EventBus.getDefault().unregister( this );
	}

	private void onRefresh()
	{
		Log.d( TAG, "onRefresh" );
		adapter.refreshNews();
	}

	@SuppressWarnings( "unused" )
	@Subscribe( threadMode = ThreadMode.MAIN )
	public void onQueryUpdate( HomeActivity.QueryUpdate queryUpdate )
	{
		adapter.fetchNews( queryUpdate.query, 0, false, true );
	}

	private class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>
	{
		private List<Article> articles;
		private String        currentQuery;
		private int           currentPage;
		private boolean       fetchingNews;

		NewsAdapter()
		{
			articles = new ArrayList<>();
			currentPage = 0;
			fetchingNews = false;
		}

		void refreshNews()
		{
			fetchNews( currentQuery, 0, false, false );
		}

		void fetchNews( String query, int page, boolean append, boolean displayProgressBar )
		{
			if ( displayProgressBar )
				binding.progressBar.setVisibility( View.VISIBLE );
			binding.connectionInfo.setVisibility( View.GONE );

			currentQuery = query == null || query.isEmpty() ? getString( R.string.news_in_country, Locale.getDefault().getDisplayCountry() ) : query;
			currentPage = page < 0 ? 0 : page;
			fetchingNews = true;
			Helper.fetchArticles( currentQuery, currentPage )
					   .subscribeOn( Schedulers.newThread() )
					   .observeOn( AndroidSchedulers.mainThread() )
					   .subscribe( _articles -> {
						   int oldSize = articles.size();
						   if ( !append )
						   {
							   articles.clear();
							   notifyItemRangeRemoved( 0, oldSize );
							   oldSize = 0;
						   }
						   articles.addAll( _articles );
						   Log.d( TAG, "notifyItemRangeInserted: " + oldSize + ", " + _articles.size() );
						   notifyItemRangeInserted( oldSize, _articles.size() );
						   fetchingNews = false;
						   binding.swipeRefreshLayout.setRefreshing( false );
						   binding.progressBar.setVisibility( View.GONE );
					   }, throwable -> {
						   if ( throwable instanceof java.net.UnknownHostException || throwable instanceof java.net.SocketTimeoutException )
						   {
							   binding.swipeRefreshLayout.setRefreshing( false );
							   binding.progressBar.setVisibility( View.GONE );
							   binding.connectionInfo.setVisibility( View.VISIBLE );
						   }
						   else
							   Log.e( TAG, "query="+currentQuery+", page="+currentPage+", append="+append, throwable );
					   } );
		}

		@Override
		public NewsViewHolder onCreateViewHolder( ViewGroup parent, int viewType )
		{
			return new NewsViewHolder( DataBindingUtil.bind( LayoutInflater.from( parent.getContext() ).inflate( R.layout.view_item_news, parent, false ) ) );
		}

		@Override
		public void onBindViewHolder( NewsViewHolder holder, int position )
		{
			holder.getBinding().setArticle( articles.get( position ) );
			if ( position == articles.size() - 1 && !fetchingNews )
				fetchNews( currentQuery, currentPage + 1, true, true );
		}

		@Override
		public int getItemCount()
		{
			return articles.size();
		}

		class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
		{
			private ViewItemNewsBinding binding;

			NewsViewHolder( ViewItemNewsBinding binding )
			{
				super( binding.getRoot() );
				this.binding = binding;
				this.binding.getRoot().setOnClickListener( this );
			}

			ViewItemNewsBinding getBinding()
			{
				return binding;
			}

			@Override
			public void onClick( View v )
			{
				binding.getArticle().save();
				Intent intent = new Intent( getContext(), ArticleActivity.class );
				intent.putExtra( "url", binding.getArticle().getUrl() );
				startActivity( intent );
			}
		}
	}
}
