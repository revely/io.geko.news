package io.geko.news.pages.home;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import io.geko.news.R;
import io.geko.news.databinding.ActivityHomeBinding;
import io.geko.news.pages.home.fragments.news.NewsFragment;
import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity
{
	@SuppressWarnings( "unused" )
	public static final String TAG = HomeActivity.class.getSimpleName();

	private ActivityHomeBinding binding;

	private SectionsPagerAdapter sectionsPagerAdapter;

	public static class QueryUpdate
	{
		public final String query;
		public QueryUpdate( String query ) {this.query = query;}
	}

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		binding = DataBindingUtil.setContentView( this, R.layout.activity_home );

		sectionsPagerAdapter = new SectionsPagerAdapter( getSupportFragmentManager() );
		binding.container.setAdapter( sectionsPagerAdapter );

		binding.search.setOnEditorActionListener( ( v, actionId, event ) -> {
			if ( actionId == EditorInfo.IME_ACTION_SEARCH
					|| actionId == EditorInfo.IME_ACTION_DONE
					|| event.getAction() == KeyEvent.ACTION_DOWN
					&& event.getKeyCode() == KeyEvent.KEYCODE_ENTER )
			{
				String query = binding.search.getText().toString();
				if ( query.isEmpty() )
					binding.search.setText( getString( R.string.news_in_country, Locale.getDefault().getDisplayCountry() ) );
				EventBus.getDefault().post( new QueryUpdate( query ) );
				return true;
			}
			return false;
		} );

		binding.searchIcon.setOnClickListener( view -> {
			binding.search.requestFocus();
			InputMethodManager inputMethodManager = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
			inputMethodManager.showSoftInput( binding.search, 0 );
		} );
		binding.searchClose.setOnClickListener( this::onClickSearchClose );

		binding.search.addTextChangedListener( new TextWatcher() {
			@Override
			public void beforeTextChanged( CharSequence s, int start, int count, int after ) {}

			@Override
			public void onTextChanged( CharSequence s, int start, int before, int count ) {}

			@Override
			public void afterTextChanged( Editable s )
			{
				boolean haveText = binding.search.getText().toString().length() > 0;
				binding.searchClose.setVisibility( haveText ? View.VISIBLE : View.GONE );
				binding.searchIcon.setVisibility( haveText ? View.GONE : View.VISIBLE );
			}
		} );

		binding.search.setText( getString( R.string.news_in_country, Locale.getDefault().getDisplayCountry() ) );
		binding.getRoot().postDelayed( () -> EventBus.getDefault().post( new QueryUpdate( binding.search.getText().toString() ) ), 50 );
	}

	public void onClickSearchClose( View view )
	{
		binding.search.setText( "" );
		binding.search.requestFocus();
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
		inputMethodManager.showSoftInput( binding.search, 0 );
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter
	{
		public SectionsPagerAdapter( FragmentManager fm )
		{
			super( fm );
		}

		@Override
		public Fragment getItem( int position )
		{
			if ( position == 0 )
				return NewsFragment.newInstance();
			return null;
		}

		@Override
		public int getCount()
		{
			return 1;
		}

		@Override
		public CharSequence getPageTitle( int position )
		{
			switch ( position )
			{
				case 0:
					return "NEWS";
			}
			return null;
		}
	}
}
