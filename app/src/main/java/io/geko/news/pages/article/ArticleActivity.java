package io.geko.news.pages.article;

import android.annotation.TargetApi;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import io.geko.news.R;
import io.geko.news.bindings.variables.Article;
import io.geko.news.databinding.ActivityArticleBinding;
import io.geko.news.services.Helper;
import io.geko.news.utils.FontCache;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import org.jsoup.nodes.Element;

import java.util.Scanner;

/**
 * Created at 24/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class ArticleActivity extends AppCompatActivity
{
	@SuppressWarnings( "unused" )
	public static final String TAG = ArticleActivity.class.getSimpleName();

	private ActivityArticleBinding binding;
	private String url;
	private String fragment;

	@Override
	protected void onCreate( @Nullable Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );

		Typeface typeface = FontCache.getInstance( this ).get( "open_sans_cond_light" );
		binding = DataBindingUtil.setContentView( this, R.layout.activity_article );
		binding.collapsingToolbar.setCollapsedTitleTextAppearance( R.style.CollapsedAppBar );
		binding.collapsingToolbar.setExpandedTitleTextAppearance( R.style.ExpandedAppBar );
		binding.collapsingToolbar.setCollapsedTitleTypeface( typeface );
		binding.collapsingToolbar.setExpandedTitleTypeface( typeface );

		binding.webView.getSettings().setJavaScriptEnabled( true );
		binding.webView.setWebViewClient( new WebViewClient() {
			@SuppressWarnings( "deprecation" )
			@Override
			public boolean shouldOverrideUrlLoading( WebView view, String url )
			{
				openUrl( url );
				return true;
			}

			@TargetApi( Build.VERSION_CODES.N )
			@Override
			public boolean shouldOverrideUrlLoading( WebView view, WebResourceRequest webResourceRequest )
			{
				openUrl( webResourceRequest.getUrl().toString() );
				return true;
			}
		} );

		url = getIntent().getStringExtra( "url" );

		if ( url == null )
			url = getIntent().getDataString();

		Uri uri = Uri.parse( url );
		url = uri.getScheme() + ":" + uri.getEncodedSchemeSpecificPart();
		fragment = uri.getEncodedFragment();

		Log.d( TAG, "Open: " + url + ", with fragment: #" + fragment );

		if ( binding.getArticle() == null )
		{
			Article savedArticle = Article.getSaved( url );
			if ( savedArticle == null )
				savedArticle = Article.get( url );
			setArticle( savedArticle );
			if ( savedArticle.getContent() == null )
			{
				Log.d( TAG, "Load content: " + url );
				Helper.fetchArticle( url )
					  .subscribeOn( Schedulers.newThread() )
					  .observeOn( AndroidSchedulers.mainThread() )
					  .subscribe( this::setArticle, throwable -> {
						  binding.progressBar.setVisibility( View.GONE );
						  binding.webView.loadUrl( url );
						  Log.e( TAG, "fetchArticle", throwable );
					  } );
			}
		}
		else
			setArticle( binding.getArticle() );
	}

	private void setArticle( Article article )
	{
		article.save();
		binding.setArticle( article );

		int width = binding.webView.getMeasuredWidth();
		String style = new Scanner( getResources().openRawResource( R.raw.web_view ), "UTF-8" ).useDelimiter( "\\A" ).next();

		Element html = new Element( "html" );
		Element head = new Element( "head" );
		head.append( "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" );
		head.append( "<meta name=\"viewport\"content=\"width=" + width + ", initial-scale=0.65\" />" );
		head.append( "<style type=\"text/css\">" + style + "</style>" );
		html.appendChild( head );
		if ( article.getContent() != null )
		{
			binding.progressBar.setVisibility( View.GONE );
			html.append( article.getContent() );
		}

		binding.webView.loadDataWithBaseURL( "", html.toString(), "text/html; charset=UTF-8", null, null );

		boolean havePicture = article.getPicture() != null && !article.getPicture().isEmpty();
		binding.appbar.setExpanded( havePicture, false );
		ViewCompat.setNestedScrollingEnabled( binding.nestedScrollView, havePicture );
	}

	public void onClickBack( View view )
	{
		finish();
	}

	private void openUrl( String url )
	{
		Intent intent = new Intent( ArticleActivity.this, ArticleActivity.class );
		intent.putExtra( "url", url );
		startActivity( intent );
	}
}
