package io.geko.news.utils;

import android.content.Context;
import io.geko.news.R;

/**
 * Created at 24/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class Utils
{
	private static final int SECOND_MILLIS = 1000;
	private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
	private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
	private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

	public static String getTimeAgo( Context context, long time )
	{
		if ( time < 1000000000000L )
		{
			// if timestamp given in seconds, convert to millis
			time *= 1000;
		}

		long now = System.currentTimeMillis();
		if ( time > now || time <= 0 )
			return null;

		// TODO: localize
		final long diff = now - time;
		if ( diff < MINUTE_MILLIS )
			return context.getString( R.string.just_now );
		else if ( diff < 2 * MINUTE_MILLIS )
			return context.getString( R.string.a_minute_ago );
		else if ( diff < 50 * MINUTE_MILLIS )
			return context.getString( R.string.x_minute_ago, diff / MINUTE_MILLIS );
		else if ( diff < 90 * MINUTE_MILLIS )
			return context.getString( R.string.an_hour_ago );
		else if ( diff < 24 * HOUR_MILLIS )
			return context.getString( R.string.hours_ago, diff / HOUR_MILLIS );
		else if ( diff < 48 * HOUR_MILLIS )
			return context.getString( R.string.yesterday );
		else
			return context.getString( R.string.days_ago, diff / DAY_MILLIS );
	}
}
