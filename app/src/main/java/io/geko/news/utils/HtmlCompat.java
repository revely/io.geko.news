package io.geko.news.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.text.Html;
import android.text.Spanned;

/**
 * Created at 29/09/16
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class HtmlCompat
{
	public static Spanned fromHtml( Context context, @StringRes int textRes )
	{
		return fromHtml( context.getString( textRes ) );
	}

	public static Spanned fromHtml( String text )
	{
		if ( text == null )
			return null;
		if ( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N )
			return Html.fromHtml( text, Html.FROM_HTML_MODE_LEGACY );
		else
			return Html.fromHtml( text );
	}
}
