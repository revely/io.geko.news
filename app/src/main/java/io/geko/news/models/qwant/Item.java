
package io.geko.news.models.qwant;

import android.util.Log;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.geko.news.bindings.variables.Article;

import java.util.Date;
import java.util.List;

public class Item
{

	@SerializedName( "title" )
	@Expose
	private String  title;
	@SerializedName( "url" )
	@Expose
	private String  url;
	@SerializedName( "desc" )
	@Expose
	private String  desc;
	@SerializedName( "date" )
	@Expose
	private Integer date;
	@SerializedName( "domain" )
	@Expose
	private String  domain;
	@SerializedName( "_id" )
	@Expose
	private String  id;
	@SerializedName( "media" )
	@Expose
	private List<Medium> media = null;

	public String getTitle()
	{
		return title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl( String url )
	{
		this.url = url;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc( String desc )
	{
		this.desc = desc;
	}

	public Integer getDate()
	{
		return date;
	}

	public void setDate( Integer date )
	{
		this.date = date;
	}

	public String getDomain()
	{
		return domain;
	}

	public void setDomain( String domain )
	{
		this.domain = domain;
	}

	public String getId()
	{
		return id;
	}

	public void setId( String id )
	{
		this.id = id;
	}

	public List<Medium> getMedia()
	{
		return media;
	}

	public void setMedia( List<Medium> media )
	{
		this.media = media;
	}

	public Article toArticle()
	{
		Article article = Article.get( url );
		article.setId( id );
		article.setTitle( title );
		article.setDesc( desc );
		article.setDomain( domain );
		article.setDate( new Date( (long)date * 1000 ) );
		if ( media.size() >= 1 )
		{
			article.setPicture( "https:" + media.get( 0 ).getPict().getUrl() );
			article.setPictureBig( "https:" + media.get( 0 ).getPictBig().getUrl() );
		}
		return article;
	}
}
