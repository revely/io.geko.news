
package io.geko.news.models.qwant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Medium {

    @SerializedName("pict")
    @Expose
    private Pict pict;
    @SerializedName("pict_big")
    @Expose
    private PictBig pictBig;

    public Pict getPict() {
        return pict;
    }

    public void setPict(Pict pict) {
        this.pict = pict;
    }

    public PictBig getPictBig() {
        return pictBig;
    }

    public void setPictBig(PictBig pictBig) {
        this.pictBig = pictBig;
    }

}
