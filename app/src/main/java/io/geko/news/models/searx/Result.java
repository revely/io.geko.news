
package io.geko.news.models.searx;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import io.geko.news.bindings.variables.Article;

import java.util.List;

public class Result
{
	@SerializedName( "engine" )
	@Expose
	private String engine;
	@SerializedName( "pubdate" )
	@Expose
	private String pubdate;
	@SerializedName( "engines" )
	@Expose
	private List<String> engines = null;
	@SerializedName( "publishedDate" )
	@Expose
	private String publishedDate;
	@SerializedName( "url" )
	@Expose
	private String url;
	@SerializedName( "positions" )
	@Expose
	private List<Integer> positions = null;
	@SerializedName( "parsed_url" )
	@Expose
	private List<String>  parsedUrl = null;
	@SerializedName( "content" )
	@Expose
	private String content;
	@SerializedName( "pretty_url" )
	@Expose
	private String prettyUrl;
	@SerializedName( "score" )
	@Expose
	private Double score;
	@SerializedName( "title" )
	@Expose
	private String title;
	@SerializedName( "img_src" )
	@Expose
	private String imgSrc;
	@SerializedName( "template" )
	@Expose
	private String template;
	@SerializedName( "thumbnail" )
	@Expose
	private String thumbnail;

	public String getEngine()
	{
		return engine;
	}

	public void setEngine( String engine )
	{
		this.engine = engine;
	}

	public String getPubdate()
	{
		return pubdate;
	}

	public void setPubdate( String pubdate )
	{
		this.pubdate = pubdate;
	}

	public List<String> getEngines()
	{
		return engines;
	}

	public void setEngines( List<String> engines )
	{
		this.engines = engines;
	}

	public String getPublishedDate()
	{
		return publishedDate;
	}

	public void setPublishedDate( String publishedDate )
	{
		this.publishedDate = publishedDate;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl( String url )
	{
		this.url = url;
	}

	public List<Integer> getPositions()
	{
		return positions;
	}

	public void setPositions( List<Integer> positions )
	{
		this.positions = positions;
	}

	public List<String> getParsedUrl()
	{
		return parsedUrl;
	}

	public void setParsedUrl( List<String> parsedUrl )
	{
		this.parsedUrl = parsedUrl;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent( String content )
	{
		this.content = content;
	}

	public String getPrettyUrl()
	{
		return prettyUrl;
	}

	public void setPrettyUrl( String prettyUrl )
	{
		this.prettyUrl = prettyUrl;
	}

	public Double getScore()
	{
		return score;
	}

	public void setScore( Double score )
	{
		this.score = score;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public String getImgSrc()
	{
		return imgSrc;
	}

	public void setImgSrc( String imgSrc )
	{
		this.imgSrc = imgSrc;
	}

	public String getTemplate()
	{
		return template;
	}

	public void setTemplate( String template )
	{
		this.template = template;
	}

	public String getThumbnail()
	{
		return thumbnail;
	}

	public void setThumbnail( String thumbnail )
	{
		this.thumbnail = thumbnail;
	}

	public Article toArticle()
	{
		Article article = Article.get( url );
		article.setTitle( title );
		article.setDesc( content );
//		article.setDate( pubdate );
		return article;
	}
}
