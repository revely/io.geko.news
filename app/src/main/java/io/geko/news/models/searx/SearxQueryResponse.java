
package io.geko.news.models.searx;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearxQueryResponse
{
	@SerializedName( "number_of_results" )
	@Expose
	private Integer numberOfResults;
	@SerializedName( "suggestions" )
	@Expose
	private List<Object> suggestions = null;
	@SerializedName( "results" )
	@Expose
	private List<Result> results     = null;
	@SerializedName( "answers" )
	@Expose
	private List<Object> answers     = null;
	@SerializedName( "corrections" )
	@Expose
	private List<Object> corrections = null;
	@SerializedName( "query" )
	@Expose
	private String query;
	@SerializedName( "infoboxes" )
	@Expose
	private List<Object> infoboxes = null;

	public Integer getNumberOfResults()
	{
		return numberOfResults;
	}

	public void setNumberOfResults( Integer numberOfResults )
	{
		this.numberOfResults = numberOfResults;
	}

	public List<Object> getSuggestions()
	{
		return suggestions;
	}

	public void setSuggestions( List<Object> suggestions )
	{
		this.suggestions = suggestions;
	}

	public List<Result> getResults()
	{
		return results;
	}

	public void setResults( List<Result> results )
	{
		this.results = results;
	}

	public List<Object> getAnswers()
	{
		return answers;
	}

	public void setAnswers( List<Object> answers )
	{
		this.answers = answers;
	}

	public List<Object> getCorrections()
	{
		return corrections;
	}

	public void setCorrections( List<Object> corrections )
	{
		this.corrections = corrections;
	}

	public String getQuery()
	{
		return query;
	}

	public void setQuery( String query )
	{
		this.query = query;
	}

	public List<Object> getInfoboxes()
	{
		return infoboxes;
	}

	public void setInfoboxes( List<Object> infoboxes )
	{
		this.infoboxes = infoboxes;
	}
}
