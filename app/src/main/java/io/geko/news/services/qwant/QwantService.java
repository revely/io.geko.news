package io.geko.news.services.qwant;

import io.geko.news.models.qwant.QwantQueryResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created at 26/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public interface QwantService
{
	//https://api.qwant.com/api/search/news?q=Google&locale=fr_FR&order=date&count=10&offset=0&source=all
	@GET( "news" )
	Observable<QwantQueryResponse> query( @Query( "q" ) String query,
										  @Query( "locale" ) String locale,
										  @Query( "order" ) String order,
										  @Query( "count" ) int count,
										  @Query( "offset" ) int offset );
}
