package io.geko.news.services;

import io.geko.news.bindings.variables.Article;
import io.geko.news.services.mercury.MercuryHelper;
import io.geko.news.services.qwant.QwantHelper;
import io.reactivex.Observable;

import java.util.List;

/**
 * Created at 26/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class Helper
{
	private static final INews    NEWS_IMPL = new QwantHelper();
	private static final IArticle ARTICLE_IMPL = new MercuryHelper();

	public interface INews
	{
		Observable<List<Article>> fetchArticles( String query, int page );
	}

	public interface IArticle
	{
		Observable<Article> fetchArticle( String url );
	}

	public static Observable<List<Article>> fetchArticles( String query, int page )
	{
		return NEWS_IMPL.fetchArticles( query, page );
	}

	public static Observable<Article> fetchArticle( String url )
	{
		return ARTICLE_IMPL.fetchArticle( url );
	}
}
