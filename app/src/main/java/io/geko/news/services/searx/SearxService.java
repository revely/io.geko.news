package io.geko.news.services.searx;

import io.geko.news.models.searx.SearxQueryResponse;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created at 22/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public interface SearxService
{
	@FormUrlEncoded
	@POST( "/" )
	Observable<SearxQueryResponse> query(
			@Field( "q" ) String query,
			@Field( "pageno" ) int page,
			@Field( "language" ) String language,
			@Field( "category_news" ) String categoryNews,
			@Field( "format" ) String format );

	@FormUrlEncoded
	@POST( "autocompleter" )
	Observable<SearxQueryResponse> autocompleter(
			@Field( "q" ) String query );
}
