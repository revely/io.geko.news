package io.geko.news.services.searx;

import io.geko.news.NewsApp;
import io.geko.news.bindings.variables.Article;
import io.geko.news.models.searx.Result;
import io.geko.news.models.searx.SearxQueryResponse;
import io.geko.news.services.Helper;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created at 24/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class SearxHelper implements Helper.INews
{
	public Observable<List<Article>> fetchArticles( String query, int page )
	{
		page += 1;
		return query( query, page ).map( searxQueryResponse -> {
			List<Article> articles = new ArrayList<>();
			for ( Result result : searxQueryResponse.getResults() )
				articles.add( result.toArticle() );
			return articles;
		} );
	}

	private static Observable<SearxQueryResponse> query( String query, int page )
	{
		return query( query, page, Locale.getDefault().toString() );
	}

	private static Observable<SearxQueryResponse> query( String query, int page, String language )
	{
		return NewsApp.getSearxService().query( query, page, language, "on", "json" );
	}
}
