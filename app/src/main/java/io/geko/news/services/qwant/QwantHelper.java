package io.geko.news.services.qwant;

import io.geko.news.NewsApp;
import io.geko.news.bindings.variables.Article;
import io.geko.news.models.qwant.Item;
import io.geko.news.models.qwant.QwantQueryResponse;
import io.geko.news.services.Helper;
import io.reactivex.Observable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created at 26/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class QwantHelper implements Helper.INews
{
	public Observable<List<Article>> fetchArticles( String query, int page )
	{
		return query( query, page ).map( qwantQueryResponse -> {
			List<Article> articles = new ArrayList<>();
			for ( Item item : qwantQueryResponse.getData().getResult().getItems() )
				articles.add( item.toArticle() );
			return articles;
		} );
	}

	public static Observable<QwantQueryResponse> query( String query, int page )
	{
		return NewsApp.getQwantService().query( query, Locale.getDefault().toString(), "date", 10, page * 10 );
	}
}
