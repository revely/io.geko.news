package io.geko.news.services.mercury;

import io.geko.news.models.MercuryParserResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created at 24/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public interface MercuryService
{
	@GET( "parser" )
	Observable<MercuryParserResponse> parser( @Query( "url" ) String url );
}
