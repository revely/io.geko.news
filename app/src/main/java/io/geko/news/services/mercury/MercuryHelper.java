package io.geko.news.services.mercury;

import android.util.Log;
import io.geko.news.NewsApp;
import io.geko.news.bindings.variables.Article;
import io.geko.news.models.MercuryParserResponse;
import io.geko.news.services.Helper;
import io.reactivex.Observable;


/**
 * Created at 24/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class MercuryHelper implements Helper.IArticle
{
	@SuppressWarnings( "unused" )
	public static final String TAG = MercuryHelper.class.getSimpleName();

	@Override
	public Observable<Article> fetchArticle( String url )
	{
		return extract( url ).map( mercuryParserResponse -> {
			Article article = Article.get( mercuryParserResponse.getUrl() );
			Log.d( TAG, "mercuryParserResponse: " + NewsApp.GSON.toJson( article ) );

			article.setContent( mercuryParserResponse.getContent() );
			article.setAuthor( mercuryParserResponse.getAuthor() );
			if ( mercuryParserResponse.getLeadImageUrl() == null || mercuryParserResponse.getLeadImageUrl().isEmpty() )
				article.setPicture( mercuryParserResponse.getLeadImageUrl() );
			if ( article.getTitle() == null || article.getTitle().isEmpty() )
				article.setTitle( mercuryParserResponse.getTitle() );

			return article;
		} );
	}

	private static Observable<MercuryParserResponse> extract( String url )
	{
		return NewsApp.getMercuryService().parser( url );
	}
}
