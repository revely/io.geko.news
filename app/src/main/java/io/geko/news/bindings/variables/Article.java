package io.geko.news.bindings.variables;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import io.geko.news.NewsApp;

import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * Created at 26/04/2017
 * <I need to be documented !>
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class Article
{
	public static final String URL_TO_ARTICLE_ = "URL_TO_ARTICLE_";

	private String id;

	private String title;

	private String url;

	private String desc;

	private String author;

	private String picture;

	private String pictureBig;

	private String domain;

	private String content;

	private Date date;

	public Article(){}

	@NonNull
	public static Article get( String url )
	{
		String jsonArticle = NewsApp.getSharedPreferences().getString( URL_TO_ARTICLE_ + url, null );
		if ( jsonArticle != null )
			return NewsApp.GSON.fromJson( jsonArticle, Article.class );
		Article article = new Article();
		article.setUrl( url );
		return article;
	}

	public String getId()
	{
		return id;
	}

	public void setId( String id )
	{
		this.id = id;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle( String title )
	{
		this.title = title;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl( String url )
	{
		this.url = url;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc( String desc )
	{
		this.desc = desc;
	}

	public String getAuthor()
	{
		return author;
	}

	public void setAuthor( String author )
	{
		this.author = author;
	}

	public String getPicture()
	{
		return picture;
	}

	public void setPicture( String picture )
	{
		this.picture = picture;
	}

	public String getPictureBig()
	{
		return pictureBig;
	}

	public void setPictureBig( String pictureBig )
	{
		this.pictureBig = pictureBig;
	}

	public String getDomain()
	{
		return domain;
	}

	public void setDomain( String domain )
	{
		this.domain = domain;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent( String content )
	{
		this.content = content;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate( Date date )
	{
		this.date = date;
	}

	@Nullable
	public static Article getSaved( String url )
	{
		String jsonArticle = NewsApp.getSharedPreferences().getString( URL_TO_ARTICLE_ + url, null );
		if ( jsonArticle != null )
		{
			Log.d( TAG, "Load saved article: " + url );
			return NewsApp.GSON.fromJson( jsonArticle, Article.class );
		}
		return null;
	}

	public void save()
	{
		Log.d( TAG, "Save article: " + url );
		NewsApp.getSharedPreferences().edit().putString( URL_TO_ARTICLE_ + url, NewsApp.GSON.toJson( this ) ).apply();
	}
}
