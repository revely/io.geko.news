package io.geko.news.bindings;

import android.databinding.BindingAdapter;
import android.net.Uri;
import android.widget.TextView;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import io.geko.news.utils.FontCache;
import io.geko.news.utils.HtmlCompat;
import io.geko.news.utils.Utils;

import java.util.Date;

/**
 * Created at 19/09/16
 * Binding adapters.
 *
 * @author rbenjami
 * @since 1.0.0
 */
public class Bindings
{
	@SuppressWarnings( "unused" )
	public static final String TAG = Bindings.class.getSimpleName();

	@BindingAdapter( {"android:text"} )
	public static void setDateText( TextView textView, Date date )
	{
		if ( date != null )
		{
			textView.setText( Utils.getTimeAgo( textView.getContext(), date.getTime() ) );
		}
	}

	@BindingAdapter( {"font"} )
	public static void setFont( TextView textView, String fontName )
	{
		textView.setTypeface( FontCache.getInstance( textView.getContext() ).get( fontName ) );
	}

	@BindingAdapter( {"html_text"} )
	public static void setHtmlText( TextView textView, String htmlText )
	{
		textView.setText( HtmlCompat.fromHtml( htmlText ) );
	}

	@BindingAdapter( {"url"} )
	public static void setUrl( SimpleDraweeView simpleDraweeView, String url )
	{
		if ( url == null || url.isEmpty() )
		{
			simpleDraweeView.setImageURI( "" );
			return;
		}

		ImageRequest request = ImageRequestBuilder.newBuilderWithSource( Uri.parse( url ) )
												  .setProgressiveRenderingEnabled( true )
												  .build();

		DraweeController controller = Fresco.newDraweeControllerBuilder()
											.setImageRequest( request )
//											.setAutoPlayAnimations( true )
											.setOldController( simpleDraweeView.getController() )
//											.setControllerListener( new BaseControllerListener<ImageInfo>()
//											{
//												@Override
//												public void onFinalImageSet( String id, ImageInfo imageInfo, Animatable animatable )
//												{
//													super.onFinalImageSet( id, imageInfo, animatable );
//													simpleDraweeView.post( () -> simpleDraweeView.setAspectRatio( (float) imageInfo.getWidth() / imageInfo.getHeight() ) );
//												}
//											} )
											.build();
		simpleDraweeView.setController( controller );
	}
}